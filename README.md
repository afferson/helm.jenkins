# Instalação do Jenkins via Helm no Kubernetes

---

## Requisitos:

1. Acesso ao ambiente do Kubernetes de Infra;
2. Comando do Kubectl;
3. Comando Helm Client, com a mesma versão da instalada no cluster;
4. Ambiente do Kubernetes com o Tiller instalado;
5. Clone deste repositório em sua máquina.

---

## Instalação:

1. Realize o clone desse repositório em sua máquina.
2. Na pasta do projeto, existe um arquivo chamado `values.yml` execute o seguinte comando:

```
 helm install stable/jenkins \
    --name jenkins \
    --namespace jenkins \
    --values values.yml
```


Comandos adicionais:

```
helm ls
helm status jenkins
kubectl -n kube-system get cm | grep jenkins
kubectl -n kube-system describe cm jenkins.v1
helm inspect stable/jenkins
```

Removendo a instalação:

```
helm delete jenkins
helm del --purge jenkins
helm ls
```


